Graphlet analysis code
======================
We provide a python script for performing Neighborhood Graph Analysis (NGA) 
via graphlet decompositions for characterizing crystal structures on molecular 
simulation snapshots. Our script is modified and rely on a standalone package 
crayon for computing NGA based on following paper:(1)Reinhart, W. F.; Panagiotopoulos, 
A. Z. Automated Crystal Characterization with a Fast Neighborhood Graph Analysis Method. 
Soft Matter 2018, 14, 6083–6089. https://doi.org/10.1039/c8sm00960k. (2)crayon python 
module available at https://github.com/wfreinhart/crayon. To obtain the compositional NGA,
the crayon source code need to be modified, the instruction is provided in modification directory.      

Installation
============
The crayon package need to be pre-installed.

Necessary setup
---------------

Please read this file in full before proceeding.  Setup is unfortunately
somewhat complicated because of all of the dependencies which need to be
installed.  Hopefully the following should work automatically.  You need
Python 2; if you are using Anaconda you may wish to create a new environment
with something like:

    $ conda create --name example_name python=2.7
    $ conda activate example_name

You should install the dependencies manually or with a command like:

    $ conda install boost cmake matplotlib mpi4py numpy scipy sphinx

If everything is set up correctly, the command

    $ ./setup.sh

should build everything and move everything into place.


Optional setup
--------------

Now to use the code, you first need to build a library from template
structures.  Some samples are given in the `templates/block` directory, along
with some scripts to generate them from the unit cells in `templates/unit`.  You
need to have Python 3 along with PyPhase available to run those scripts (they
will be looking for `crystool` on the system path).  From the templates
directory, after updating `make.py`, you could run:

    $ ./make.py > make.sh
    $ chmod +x make.sh
    $ ./make.sh

to regenerate the templates in `templates/block`.

However, this is not required to use the templates in `templates/block` as is.
These scripts may be useful if you want to add more templates as input.

There are also templates in `templates/gdv`, as `.gdv` files.  These are
described later on in this document.


Example of running the code
---------------------------

To build a library from template structures:

    $ ./analysis.py template templates/block example_library.dat

This takes all `.xyz` files in the directory `templates/block` and constructs an
`example_library.dat` file which contains all necessary information about those
templates.  Note that `.gdv` files will also be searched for after `.xyz`
files.  These are described later on.

Now to do analysis on some trajectory using that library:

    $ ./analysis.py analyze example_library.dat example/path/to/trajectory.xyz
      example_output -b 30 30 30 -f 0 10001 1000 -r 1 0.3 -r 2 0.5

The parameters after the `-b` option are the box dimensions in x, y, z.  This is
necessary for XYZ files that come out of LAMMPS since the box information is
not stored in the header.  The parameters after `-f`, here for this example `0
10001 1000` specify a `range(0, 10001, 1000)` of frames to read.  Leaving this
out reads all frames which may take a long time.  In this case, the frames
read are 0, 1000, 2000, ..., 10000.  Finally, each `-r` specifies a mapping from
particle types in the XYZ file to radii.  This is not used in the analysis
itself and incorrect values will not affect structural classification.  This
is only used for visualization output (these data are stored so that viewing
the results in OVITO gives the correct particle sizes).

Here the output is specified as `example_output`.  This will create the
following files:

*   `example_output.txt`: Text data giving the particle classification counts.

*   `example_output.gdv`: Text data giving the signatures of all structures
    found.  Discussed in more detail below.

*   `example_output_sig.xyz`: File which can be visualized as:

        $ ovito example_output_sig.xyz

    which will display color coding according to the classifications.  The
    classification is also mapped to the `StructureType` attribute which can
    be used for filtering particles through modifiers to look at particles
    from a specific structure.

There are a few OVITO bugs which may result in problems when viewing
`example_output_sig.xyz`.  The colors should not be incorrect (if they are, this
is probably a bug in the analysis script), but be aware that:

*   You need to play through to the end of the trajectory if there are
    multiple frames with different classifications occurring within them, or
    OVITO will not display the full list.

*   OVITO does not show the colors properly in the `StructureType` list.

*   If using the `SelectParticleTypeModifier`, and a classification with count
    equal to zero is selected, this sometimes selects another particle type
    with particles in it and two checkboxes appear checked even though only one
    is selected.  Unfortunately the only way around this that I am absolutely
    sure of is to defer to the `.txt` file, which should always contain
    absolutely correct particle counts unless there is a problem in the
    analysis algorithm itself.

I do not have a good way around this, although in theory one can filter and
export separate `.xyz` files for each classification.

For more details on use, run:

    $ ./analysis.py --help
    $ ./analysis.py template --help
    $ ./analysis.py analyze --help

There are additional options which can adjust various behaviors; these are
documented within.

The analysis produces `all.gdv` files containing "graphlet degree vectors" (GDVs)
representing uniquely the different structural signatures which are found.
These vectors are 73-component vectors of non-negative integers. `.gdv` files
produced as output contain GDVs for the unknown structures, which can be used
in conjunction with the `.xyz` files to select particular signatures for
structures of interest. The analysis also procudes `same.gdv` `diff.gdv` for same type and 
different types particles. These signatures can be used further to identify the 
identity of binary superlattices along with autoencoder. 

Using `.gdv` files as templates
-------------------------------
One or more vectors can then be placed in a `.gdv` file and used as a template
directly; in this way, it is not necessary to try to construct the unit cell
of a structure of interest.  See `templates/gdv` for some examples.  All
vectors in a given file will be read in and collected under a single template
structure. 

Finally, keep in mind that the `.gdv` files' contents are dependent upon the
particular settings of `-r/--r-cut` and `-n/--n-shells` used during the
templating stage.  Reading in a `.gdv` where these options do not match may
yield nonsensical results!  Be careful if these options are being employed.
