#!/usr/bin/env bash
crystool rescale unit/SC.dat scaled/SC.dat -r 0.514
crystool tile scaled/SC.dat block/SC.xyz 8 8 8
crystool rescale unit/SH.dat scaled/SH.dat -r 0.514
crystool tile scaled/SH.dat block/SH.xyz 6 6 6
crystool rescale unit/BCC.dat scaled/BCC.dat -r 0.514
crystool tile scaled/BCC.dat block/BCC.xyz 6 6 6
crystool rescale unit/FCC.dat scaled/FCC.dat -r 0.514
crystool tile scaled/FCC.dat block/FCC.xyz 5 5 5
crystool rescale unit/HCP.dat scaled/HCP.dat -r 0.514
crystool tile scaled/HCP.dat block/HCP.xyz 5 5 5
crystool rescale unit/AlB_2.dat scaled/AlB_2.dat -r 0.514 0.514
crystool tile scaled/AlB_2.dat block/AlB_2.xyz 4 4 4
crystool rescale unit/SiCr_3.dat scaled/SiCr_3.dat -r 0.514 0.514
crystool tile scaled/SiCr_3.dat block/SiCr_3.xyz 4 4 4
crystool rescale unit/AsNi.dat scaled/AsNi.dat -r 0.514 0.514
crystool tile scaled/AsNi.dat block/AsNi.xyz 3 3 3
