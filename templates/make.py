#!/usr/bin/env python2

from __future__ import print_function

# Name, number of components, number of repeats
DATA = [
    ("SC", 1, 8),
    ("SH", 1, 6),
    ("BCC", 1, 6),
    ("FCC", 1, 5),
    ("HCP", 1, 5),
    ("AlB_2", 2, 4),
    ("SiCr_3", 2, 4),
    ("AsNi", 2, 3)
]
RADIUS = 0.514

print("#!/usr/bin/env bash")

for name, components, repeats in DATA:
    print("crystool rescale unit/{0}.dat scaled/{0}.dat -r {1}".format(name,
        " ".join(str(RADIUS) for index in range(components))))
    print("crystool tile scaled/{0}.dat block/{0}.xyz {1} {1} {1}".format(name, repeats))
