#!/usr/bin/env bash
set -evx

DMAP="dmap"
CRAYON="crayon"
BUILD="build"
DOC="doc"
DMAPLIB="PyDMap.so"

# Make sure we are in the current directory
cd "$(dirname -- "$0")"

# Build dmap
(
    cd "$DMAP"

    # Make build directory
    if [ ! -d "$BUILD" ]; then
        mkdir "$BUILD"
    fi

    # Run build process
    (
        cd "$BUILD"
        cmake ..
        make
    )

    # Make symlink to PyDMap
    if [ ! -L "$DMAPLIB" ]; then
        ln -s "$BUILD/$DMAPLIB" .
    fi
)

# Build crayon
(
    cd "$CRAYON"
    
    # Make symlink to PyDMap
    if [ ! -L "$DMAPLIB" ]; then
        ln -s "../$DMAP/$DMAPLIB" .
    fi

    # Make build directory
    if [ ! -d "$BUILD" ]; then
        mkdir "$BUILD"
    fi
    
    # Run build process for library
    (
        cd "$BUILD"
        if [ ! -L "$DMAPLIB" ]; then
            ln -s "../$DMAPLIB" .
        fi
        cmake ..
        make
        make test
        make install
    )

    # Run build process for documentation
    (
        cd "$DOC"
        PYTHONPATH=.. make html
    )
)
