#
# __init__.py
# defines crayon module and loads submodules
#
# Copyright (c) 2018 Wesley Reinhart.
# This file is part of the crayon project, released under the Modified BSD License.

import warnings as _warnings

import _crayon
import classifiers
try:
    import dmap
except:
    _warnings.warn('Diffusion map features disabled')
import io
import neighborlist
import nga
try:
    import parallel
except:
    _warnings.warn('Parallel features disabled')
