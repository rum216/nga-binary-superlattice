from __future__ import print_function

import itertools
import numpy

# Loads a single frame from a file
def load(file, skip=False, fast=False):
    """
    Loads a frame from an XYZ file.

    Parameters
    ----------
    file : file
        The file from which to read.
    skip : bool
        When set, skips over the frame instead of parsing.  Defaults to False.
    fast : bool
        When set, skips over the comment line instead of parsing.  Defaults to False.

    Returns
    -------
    tuple
        See `dump`.
    """

    # The number of atoms always comes first
    try:
        # Blanks between frames are not allowed but sometimes show up anyway
        while True:
            atom_count = next(file).strip()
            if atom_count:
                break
        atom_count = int(atom_count)
    except StopIteration:
        # This is the only permissible point for a StopIteration and means EOF
        return
    
    if atom_count < 0:
        raise ValueError("Invalid number of atoms")
    
    # If skip=True, read over the next comment + N atom lines to the following frame
    if skip:
        next(itertools.islice(file, atom_count + 1, atom_count + 1), None)
        return
            
    # Process the comment line and retrieve information about the columns
    if not fast:
        metadata = _parse_comment(next(file).strip())
    else:
        next(file)
        metadata = {"properties": _DEFAULT_PROPERTIES}
    properties = metadata["properties"]

    # "species" and "pos" are special and this data should be first
    if properties[0][0].lower() != "species" or properties[0][1] is not str or properties[0][2] != 1 \
            or properties[1][0].lower() != "pos" or properties[1][1] is not float:
        raise ValueError("Unexpected starting columns")

    # If fast=True or the file doesn't have a property line, read assuming (type, x, y, z)
    if fast or properties == _DEFAULT_PROPERTIES:
        data = [[None] * atom_count, numpy.zeros((atom_count, 3))]
        for atom_index in range(atom_count):
            # Get the strings for each value in this row
            values = next(file).strip().split()
            data[0][atom_index] = values[0]
            # For some reason this is faster than using numpy.loadtxt
            data[1][atom_index, :] = [float(value) for value in values[1:]]
        data[0] = numpy.array(data[0])
        return _convert(data, metadata)
    
    # data holds the processed data, while instructions indicates what to do with each value
    data = []
    instructions = []

    for data_index, (key, parser, count) in enumerate(properties):
        if parser in _NUMPY_PARSERS:
            # This is a proper datatype
            data.append(numpy.zeros((atom_count, count) if count > 1 else (atom_count,), dtype=parser))
        else:
            # Something is strange about this one; assume string data
            # The maximum length will not be known until all strings are read, so use
            # Python lists to hold the strings until all have been determined
            data.append([[""] * count if count > 1 else "" for atom_index in range(atom_count)])

        # Instructions contain index into data, column for this property, and function to call for conversion
        instructions.extend((data_index, item_index if count > 1 else None, parser) for item_index in range(count))

    for atom_index in range(atom_count):
        # Get the strings for each value in this row
        fields = next(file).strip().split()
        if len(fields) != len(instructions):
            raise ValueError("Invalid number of columns {}".format(len(fields)))

        # Do the conversion
        for field, (data_index, item_index, parser) in zip(fields, instructions):
            if item_index is None:
                # One-dimensional array
                data[data_index][atom_index] = parser(field)
            else:
                # Two-dimensional array
                data[data_index][atom_index][item_index] = parser(field)
    
    # Convert string data from a list of lists to an array
    for data_index, (key, parser, count) in enumerate(properties):
        if parser not in _NUMPY_PARSERS:
            data[data_index] = numpy.array(data[data_index])

    return _convert(data, metadata)

# Loads all frames from a file
def load_all(file, fast=False):
    """
    Loads all frames from an XYZ file.

    Parameters
    ----------
    file : file
        The file from which to read.
    fast : bool
        When set, skips over the comment lines instead of parsing.  Defaults to False.

    Returns
    -------
    generator
        Generator of results from `load`.
    """

    while True:
        result = load(file, fast=fast)
        if result is None:
            break
        yield result

def dump(file, types, coordinates, lattice=None, extra_frame=None, extra_columns=None):
    """
    Saves a frame to an XYZ file.

    Parameters
    ----------
    file : file
        The file to which to write.
    types : numpy.ndarray
        An array of strings representing the atom types.
    coordinates : numpy.ndarray
        An array of floating-point values representing the atom coordinates.
    lattice : numpy.ndarray
        Lattice matrix.
    extra_frame : dict
        Key-value paired data to associated with each frame.
    extra_columns : list
        Ordered key-value paired data to add to each column.
    """

    # Header information
    print(len(types), file=file)
    
    # Populate metadata based on keyword arguments
    metadata = []

    # We want metadata in order with Lattice first, hence the use of a list instead of a dict
    if lattice is not None:
        metadata.append(("Lattice", " ".join(str(value) for value in lattice.flatten())))
    
    # Now build Properties
    if extra_columns is not None:
        extra_keys = set(key.lower() for key, data in extra_columns)
        if len(extra_keys) < len(extra_columns):
            raise ValueError("Duplicate extra property")
        if "species" in extra_keys or "pos" in extra_keys:
            raise ValueError("Forbidden extra property")
        for key, data in extra_columns:
            if data.dtype.type not in _TYPE_LETTERS:
                raise ValueError("Unrecognized type for extra property")
            if not 1 <= len(data.shape) <= 2:
                raise ValueError("Invalid shape of data for extra property")
        metadata.append(("Properties", "species:S:1:pos:R:3" + "".join(":{}:{}:{}".format(key,
            _TYPE_LETTERS[data.dtype.type], data.shape[1] if len(data.shape) > 1 else 1) for key, data in extra_columns)))
    
    # Now add any extra keys
    if extra_frame is not None:
        extra_keys = set(key.lower() for key in extra_frame)
        if len(extra_keys) < len(extra_frame):
            raise ValueError("Duplicate extra key")
        if "lattice" in extra_keys or "properties" in extra_keys:
            raise ValueError("Forbidden extra key")
        metadata.extend(sorted(extra_frame.items(), key=lambda item: item[0].lower()))

    # Extended XYZ format output
    print(*("{}={}".format(_conditional_quote(key), _conditional_quote(value))
        for key, value in metadata), file=file)

    # Data
    for index, (atom_type, position) in enumerate(zip(types, coordinates)):
        print(atom_type, *position, end=" " if extra_columns else "\n", file=file)
        if extra_columns:
            print(*(value[index] for key, data in extra_columns for value in
                (data.T if len(data.shape) > 1 else (data,))), file=file)

# Convert data and metadata into a more useful format
def _convert(data, metadata):
    metadata = dict(metadata)
    lattice = metadata.pop("lattice", None)
    properties = metadata.pop("properties", None)
    extra_columns = zip([entry[0] for entry in properties[2:]], data[2:])
    return (data[0], data[1], lattice, metadata if metadata else None,
        extra_columns if extra_columns else None)

# Parses an XYZ comment line
def _parse_comment(line):
    tokens = {}

    try:
        # Try extended XYZ?
        tokens = _parse_extended_xyz(line)
    except ValueError:
        try:
            # Try lattice vectors?
            lattice = _parse_lattice(line)
            # Set the number of coordinates based on lattice dimensionality
            tokens = {"lattice": lattice, "properties": [("species", str, 1), ("pos", float, lattice.shape[0])]}
        except ValueError:
            pass
    
    # Use the default column assignments if all else fails
    tokens.setdefault("properties", _DEFAULT_PROPERTIES)
    return tokens

# Parses an extended XYZ comment line
def _parse_extended_xyz(line):
    # Set up the parser 
    index, quoted, mark, tokens, keys = 0, False, None, {}, {}

    while True:
        try:
            # Consuming whitespace
            while line[index].isspace():
                index += 1

            # Consuming a token
            quoted, mark = False, index
            while not line[index].isspace() or quoted:
                if line[index] == "\"":
                    quoted = not quoted
                index += 1

        except IndexError:
            # End of line
            break

        finally:
            # End of token; remove quotes and get the key and value
            items = line[mark:index].replace("\"", "").split("=", 1)
            if len(items) < 2:
                raise ValueError("Key {} missing value".format(_conditional_quote(items[0])))

            # Keys should be checked case-insensitively but the real keys should remain as is
            key_lower = items[0].lower()
            if key_lower in keys:
                raise ValueError("Key {} found more than once".format(_conditional_quote(items[0])))
            tokens[items[0]] = items[1]

            # Keep a lookup from lowercase to real keys when replacing special entries
            keys[key_lower] = items[0]
    
    # Make sure that the sequence was properly terminated
    if quoted:
        raise ValueError("Closing quotation missing")

    # Handle special entries
    for key_lower, parser in (("lattice", _parse_lattice), ("properties", _parse_properties)):
        if key_lower in keys:
            # The result in the output should be lowercase
            key = keys[key_lower]
            result = parser(tokens[key])
            del tokens[key]
            tokens[key_lower] = result

    return tokens

# Quotes whitespace-containing strings
def _conditional_quote(string):
    string = str(string)
    if not string or any(character.isspace() for character in string):
        return "\"{}\"".format(string)
    return string

# Parses an XYZ or extended XYZ lattice vector line
def _parse_lattice(string):
    values = numpy.array([float(value) for value in string.strip().split()])
    count = values.shape[0]

    # Line, rectangle or cuboid
    if 1 <= count <= 3:
        return numpy.diag(values)

    # Parallelogram or parallelepiped
    if count == 4:
        return numpy.reshape(values, (2, 2))
    if count == 9:
        return numpy.reshape(values, (3, 3))
    
    # Unknown form
    raise ValueError("Lattice matrix has {} component{}".format(count, ("", "s")[count != 1]))

# Parses an extended XYZ property data string
def _parse_properties(string):
    # Property strings contain colon-separated triples
    data = string.split(":") if string else []
    if len(data) % 3 != 0:
        raise ValueError("Incorrect number of property entries")
    
    # Properties should be kept in order and keys should be checked case-insensitively
    properties, keys = [], set()
    for index in range(0, len(data), 3):
        key, kind, count = data[index:index + 3]

        # The key in the output should retain its case
        key_lower = key.lower()
        if key_lower in keys:
            raise ValueError("Property {} found more than once".format(_conditional_quote(key)))
        keys.add(key_lower)
        
        # The kind should be looked up case-insensitively
        kind_lower = kind.lower()
        if kind_lower not in _TYPE_PARSERS:
            raise ValueError("Invalid property type {}".format(_conditional_quote(kind)))
        parser = _TYPE_PARSERS[kind_lower]
        
        # There must be a positive number of fields
        count_int = int(count)
        if count_int < 0:
            raise ValueError("Invalid number of fields {}".format(_conditional_quote(count)))

        properties.append((key, parser, count_int))

    return properties

_BOOL_VALUES = {"f": False, "t": True}
_TYPE_PARSERS = {"s": str, "i": int, "r": float}
_TYPE_LETTERS = {numpy.string_: "S", numpy.int64: "I", numpy.float64: "R"}
_NUMPY_PARSERS = {int, float}
_DEFAULT_PROPERTIES = [("species", str, 1), ("pos", float, 3)]
