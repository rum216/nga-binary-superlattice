crayon package
==============

Submodules
----------

.. toctree::

   crayon.classifiers
   crayon.dmap
   crayon.io
   crayon.neighborlist
   crayon.nga
   crayon.parallel
   crayon.util

Module contents
---------------

.. automodule:: crayon
    :members:
    :undoc-members:
    :show-inheritance:
