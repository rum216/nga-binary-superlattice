crayon.parallel module
======================

.. automodule:: crayon.parallel
    :members:
    :undoc-members:
    :show-inheritance:
