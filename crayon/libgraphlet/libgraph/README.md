libgraph
========

Small utility library extending boost::graph::adjacency_matrix for working with undirected graphs.

### Compilation ###

The library itself is header only.
To compile the included conversion tool `convert` run:

```
git clone https://github.com/SimonLarsen/libgraph.git
cd libgraph
cmake . -DCMAKE_BUILD_TYPE=Release
make
```
