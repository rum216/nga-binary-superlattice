#include <iostream>
#include <graph/Graph.hpp>
#include <graph/GraphReader.hpp>
#include <graph/GraphWriter.hpp>

int main(int argc, const char *argv[]) {
	boost::adjacency_list<
		boost::setS,
		boost::vecS,
		boost::undirectedS,
		graph::LabeledVertex,
		graph::LabeledEdge,
		graph::LabeledGraph
	> g(0);

	if(argc != 3) {
		std::cerr << "error: Invalid number of arguments." << std::endl;
		std::cerr << "Usage: " << argv[0] << " INPUTFILE OUTPUTFILE" << std::endl;
		return 1;
	}

	graph::readGraph(argv[1], g);
	graph::writeGraph(g, argv[2]);

	return 0;
}
