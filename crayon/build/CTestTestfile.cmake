# CMake generated Testfile for 
# Source directory: /raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon
# Build directory: /raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_Classifiers.py "/home/rum216/miniconda2/envs/py27/bin/python" "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_Classifiers.py")
add_test(/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_Crayon.py "/home/rum216/miniconda2/envs/py27/bin/python" "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_Crayon.py")
add_test(/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_Neighborlist.py "/home/rum216/miniconda2/envs/py27/bin/python" "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_Neighborlist.py")
add_test(/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_io.py "/home/rum216/miniconda2/envs/py27/bin/python" "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_io.py")
add_test(/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_PyGraph_cpp.py "/home/rum216/miniconda2/envs/py27/bin/python" "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_PyGraph_cpp.py")
add_test(/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_Comparison_cpp.py "/home/rum216/miniconda2/envs/py27/bin/python" "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/test/test_Comparison_cpp.py")
