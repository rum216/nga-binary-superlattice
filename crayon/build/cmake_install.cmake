# Install script for directory: /raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libvoropp.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libvoropp.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libvoropp.so"
         RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libvoropp.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib" TYPE SHARED_LIBRARY FILES "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/build/libvoropp.so")
  if(EXISTS "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libvoropp.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libvoropp.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libvoropp.so"
         OLD_RPATH ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
         NEW_RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libvoropp.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/liborca.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/liborca.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/liborca.so"
         RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/liborca.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib" TYPE SHARED_LIBRARY FILES "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/build/liborca.so")
  if(EXISTS "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/liborca.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/liborca.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/liborca.so"
         OLD_RPATH ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
         NEW_RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/liborca.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libgraphlet.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libgraphlet.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libgraphlet.so"
         RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libgraphlet.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib" TYPE SHARED_LIBRARY FILES "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/build/libgraphlet.so")
  if(EXISTS "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libgraphlet.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libgraphlet.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libgraphlet.so"
         OLD_RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/build:::::"
         NEW_RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib/libgraphlet.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/_crayon.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/_crayon.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/_crayon.so"
         RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/_crayon.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon" TYPE SHARED_LIBRARY FILES "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/build/_crayon.so")
  if(EXISTS "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/_crayon.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/_crayon.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/_crayon.so"
         OLD_RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/build:::::"
         NEW_RPATH "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/_crayon.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/__init__.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/classifiers.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/dmap.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/io.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/neighborlist.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/nga.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/ovito.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/parallel.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/util.py;/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon/xyz.py")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/crayon" TYPE FILE FILES
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/__init__.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/classifiers.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/dmap.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/io.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/neighborlist.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/nga.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/ovito.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/parallel.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/util.py"
    "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/src/crayon/xyz.py"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/raid/codes/colloid/rum216/Code/08.Jagla_implicit/02.strucutre_analysis/nga-wrapper-private/crayon/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
