#!/usr/bin/env python2
# analysis: Neighborhood graph analysis using template structures

from __future__ import division
from __future__ import print_function

import argparse
import collections
import copy
import gzip
import inspect
import matplotlib.colors
import matplotlib.markers
import matplotlib.patches
import matplotlib.pyplot
import matplotlib.ticker
import numpy
import os
import pickle
import pipes
import sys

_dirname = os.path.dirname(os.path.realpath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.join(_dirname, "dmap"))
sys.path.insert(0, os.path.join(_dirname, "crayon"))
import crayon

# Cut structures from running if less than this many particles seen
ANALYZE_CUTOFF = 0 
# Cutoff for defining vapor particles. This cutoff is set as the distance where pair interaction goes to 0. 
VP_CUT = 2.5
# Define total number of A type particles in system
NA = 250  

# Output from initial analysis
AnalysisResult = collections.namedtuple("AnalysisResult", ["ensemble_data", "templates", "trajectories",
    "template_indices", "template_histogram", "backmaps", "histogram", "classifications", "unknown_count", "graphlet_vectors"])

# List of known colors and markers for plotting, and other plot parameters
COLORS = matplotlib.pyplot.rcParams["axes.prop_cycle"].by_key()["color"]

# Logging function to redirect all output to standard error
def log(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# Utility routines for persistence
def save_data(data, path):
    log("Begin save ({})".format(path))

    with gzip.open(path, "wb") as archive:
        pickle.dump(data, archive, protocol=pickle.HIGHEST_PROTOCOL)

    log("End save ({})".format(path))

def load_data(path):
    log("Begin load ({})".format(path))

    with gzip.open(path, "rb") as archive:
        result = pickle.load(archive)
        log("End load ({})".format(path))
        return result

def copy_data(data):
    return copy.deepcopy(data)

# Loads crayon.nga.Snapshot objects based on file information
def load_xyz_file(path, selector=None, box_override=None, rcut=None):
    log("Loading from file {}...".format(pipes.quote(path)))
    # neighborlist objects should be reusable
    nl = crayon.neighborlist.Voronoi()
    nl.setParams(r_max=rcut)
    nlcluster=crayon.neighborlist.Cutoff()
    nlcluster.setParams(rcut=VP_CUT)
    n_cl=[]    
    for frame_index, (frame, box, types) in enumerate(crayon.io.readXYZtraj(path)):
        # Skip some frames (if selector=None, read them all)
        if selector is not None and frame_index not in selector:
            continue
        log("  Processing frame {}...".format(frame_index))
        n_cluster=nlcluster.getNeighbors(crayon.nga.Snapshot(xyz=frame,box=box if box_override is None else box_override))
        i_cl=[len(n_cluster[i]) for i in range(len(n_cluster))]
        n_cl.append(i_cl)        
        # If box_override is None, use the box read from the XYZ file:
        # if no box could be read, box = None and box_override must be specified to proceed
        # Note we are storing types data even though crayon will not use this in analysis
        # It will be passed through and used later for outputting
        yield crayon.nga.Snapshot(xyz=frame, box=box if box_override is None else box_override, nl=nl, types=types,total_na=NA,tag=TAG)
    n_cl=numpy.array(n_cl)
    numpy.save('vapor.npy',n_cl)

# Loads crayon.classifiers.Graph objects based on file information
def load_gdv_file(path):
    log("Loading from file {}...".format(pipes.quote(path)))

    with open(path, "r") as file:
        for line in file:
            data = line.split("#")[0].strip()
            if not data:
                continue
            gdv = numpy.array([int(value) for value in data.split()], dtype=int)
            if gdv.shape != (73,):
                raise ValueError("Invalid graphlet degree vector: expected 73 components exactly")
            yield crayon.classifiers.Graph((gdv,))

# Loads snapshots into an ensemble; specifiers is a dictionary of posargs to load_file
def load_xyz_into_ensemble(ensemble, specifiers, rcut=None):
    # Inside the ensemble, keep track of all inserted IDs and store the snapshot data
    if not hasattr(ensemble, "loaded_data"):
        ensemble.loaded_data = {}
    # Sort by name so that load order should be deterministic
    for name, specifier in sorted(specifiers.items(), key=lambda item: item[0]):
        for index, frame in enumerate(load_xyz_file(*specifier, rcut=rcut)):
            key = "{}_{}".format(name, index)
            ensemble.insert(key, frame)
            del frame.adjacency # Allow for serialization
            ensemble.loaded_data.setdefault(name, []).append(frame)

# Loads GDVs directly into an ensemble
def load_gdv_into_ensemble(ensemble, specifiers, rcut=None):
    # Inside the ensemble, keep track of all inserted IDs and store the snapshot data
    if not hasattr(ensemble, "loaded_data"):
        ensemble.loaded_data = {}
    
    # Sort by name so that load order should be deterministic
    for name, specifier in sorted(specifiers.items(), key=lambda item: item[0]):
        for index, frame in enumerate(load_gdv_file(*specifier)):
            key = "{}_{}".format(name, index)
            # Set up internal data structures as necessary to insert GDV directly
            library = crayon.classifiers.GraphLibrary()
            index = library.encounter(frame)
            library.lookup[str(frame)] = numpy.zeros(1, dtype=int)
            ensemble.library.collect(library)
            ensemble.graph_lookups[key] = library.lookup
            ensemble.loaded_data.setdefault(name, []).append(frame)

# Main analysis routine
def analyze(ensemble, templates, trajectories, alpha, signature_cutoff, skip_count, amax, override=True):
    # Do the diffusion mapping
    ensemble.computeDists()
    ensemble.dmap.alpha = alpha
    ensemble.buildDMap()
    
    # Get the indices of the signatures corresponding to the template structures back;
    # also make a histogram for reference
    template_indices = {}
    template_histogram = numpy.zeros(ensemble.dists.shape[0], dtype=int)

    for key, trajectory_data in templates.items():
        for index in range(len(ensemble.loaded_data[key])):
            # Get the signature indices corresponding to each particle
            signature_indices = ensemble.backmap("{}_{}".format(key, index))

            # Accumulate the structures corresponding to each signature index
            for signature_index in signature_indices:
                template_indices.setdefault(signature_index, set()).add(key)

            # Accumulate the histogram
            template_histogram += numpy.bincount(signature_indices, minlength=template_histogram.shape[0])

    # template_indices = { signature_id: {"structure_name", ...}, ... }
    # template_histogram = [ particle_count, ... ]

    # For template indices where there is degeneracy, say that they are unknown
    bad_indices = [index for index, template_names in template_indices.items() if len(template_names) > 1]

    for bad_index in bad_indices:
        log("Unable to distinguish between some particles in {}".format(", ".join(template_indices[bad_index])))
        del template_indices[bad_index]

    if bad_indices:
        log("\x1b[93mRemoving signatures {} due to degeneracies;\n"
                "check template list and parameters if this is not OK\x1b[0m".format(bad_indices))

    select = lambda x: x # Assert that there will be no degeneracies remaining
    template_indices = {key: select(*value) for key, value in template_indices.items()}
    # template_indices = { signature_id: "structure_name" }

    # Work on each frame and accumulate histograms, as well as raw backmap data
    backmaps = {}
    histograms = []
    skip_mask = []
    for key in trajectories:
        backmaps[key] = []

        # The indices to use here will be of the range items, not the items themselves
        for index in range(len(ensemble.loaded_data[key])):
            # Get the relevant indices per particle out
            backmap = ensemble.backmap("{}_{}".format(key, index))
            backmaps[key].append(backmap)
            histograms.append(numpy.bincount(backmap, minlength=ensemble.dists.shape[0]))
            skip_mask.append(index < skip_count)

    # Normalize histogram values by number of frames
    histogram = numpy.mean(numpy.array(histograms), axis=0)
    # histogram = [ mean_count, ... ]
    histogram_max = (numpy.amax if amax else numpy.mean)(numpy.array(histograms)[numpy.logical_not(skip_mask), :], axis=0)
    # histogram contains the mean for all frames, histogram_max contains the max or mean of non-skipped frames

    # To each signature, assign a classification based on template structures and number of particles
    # -1 = below threshold, natural number = unknown but may be significant, str = template structure
    classifications = [-1] * ensemble.dists.shape[0]
    unknown_id = 0

    for index, max_count in enumerate(histogram_max):
        # Only include signatures with enough particles present
        if max_count < signature_cutoff:
            if index not in template_indices or not override:
                continue

        if index in template_indices:
            classifications[index] = template_indices[index]
        else:
            classifications[index] = unknown_id
            unknown_id += 1

    # Get graphlet vector data
    graphlet_vectors = [graph.sgdv for graph in ensemble.library.items]
    #print (graphlet_vectors) 
    # Don't save whole ensemble, just xyz
    return AnalysisResult(ensemble.loaded_data, templates, trajectories, template_indices, template_histogram,
        backmaps, histogram, classifications, unknown_id, graphlet_vectors)

# Generates tabular output based on the data
def print_table(result, table_path):
    with open(table_path, "w") as table_file:
        # All names to place in output table
        names = sorted(result.templates)

        # Maps from result.classifications to table columns
        # label >= 0: len(names) => Unknown_label; label < 0: len(names) + 1 => Skipped
        class_maps = {label: names.index(label) if isinstance(label, str)
            else len(names) + (result.unknown_count if label < 0 else label)
            for label in result.classifications}

        # Table header
        print("#", "Trajectory", "Frame", *names, end=" ", file=table_file)
        print("".join("Unknown_{} ".format(index + 1) for index in range(result.unknown_count)), end="", file=table_file)
        print("Skipped", file=table_file)

        for key, data in result.trajectories.items():
            # Extract data and try to get frame numbers
            backmap = result.backmaps[key]
            if len(data) > 1 and data[1] is not None:
                frame_indices = data[1]
            else:
                frame_indices = range(len(backmap))

            # Start outputting histograms
            for frame_index, frame in zip(frame_indices, backmap):
                table_indices = numpy.bincount([class_maps[result.classifications[item]] for item in frame],
                    minlength=len(names) + result.unknown_count + 1)
                print(key, frame_index, *table_indices, file=table_file)

# Generates a data file containing the actual graphlet vectors
def print_gdv(result, gdv_path):
	frame =0
	with open(gdv_path, "w") as gdv_file:
	    for key in result.trajectories:
		for sig in result.backmaps[key]:
			for index,type_gdv in enumerate(sig):
				if index ==0:
					print("Frame",frame ,file=gdv_file)
					frame += 1
				if isinstance(result.classifications[type_gdv], str):
					print ("#",result.classifications[type_gdv],result.graphlet_vectors[type_gdv],file=gdv_file) 
				else:
					print ("Unknown_{}".format(result.classifications[type_gdv]+1),result.graphlet_vectors[type_gdv],file=gdv_file)

# Generates a set of XYZ outputs based on the analysis
def write_xyz(result, directory, radius_map=None):

    def write_xyz_impl(filename, frames):
        with open(filename, "w") as xyz_file:
            for types, coordinates, box, payload in frames:
                crayon.xyz.dump(xyz_file, types, coordinates, lattice=numpy.diag(box), extra_columns=
                    [("radius", numpy.array([radius_map[t] for t in types]))] + payload)
    
    # Prepare for labeling and color-coding
    structure_types = sorted(set(result.classifications))
    structure_type_lookup = {value: index for index, value in enumerate(structure_types)}

    for key in result.trajectories:
        write_xyz_impl(os.path.join(directory, "{}_sig.xyz".format(key)), [(snapshot.types, snapshot.xyz, snapshot.box, [
                (header, numpy.array([function(result.classifications[sig]) for sig in result.backmaps[key][index]]))
                for header, function in (
                    ("color", lambda x: matplotlib.colors.to_rgba(COLORS[structure_type_lookup[x] % len(COLORS)])[:3]),
                    ("structure_type", lambda x: "_" + x if isinstance(x, str) else ("Skipped" if x < 0 else "Unknown_{}".format(x + 1))))
            ]) for index, snapshot in enumerate(result.ensemble_data[key])])


def load_templates(template_dir, ensemble_path, rcut, n_shells):
    # Construct template specifier dictionary
    templates_xyz, templates_gdv = {}, {}
    for file_name in os.listdir(template_dir):
        base, extension = os.path.splitext(file_name)
        # Look for all XYZ files in given directory
        for template_dict, ext in ((templates_xyz, ".xyz"), (templates_gdv, ".gdv")):
            if extension.lower() == ext:
                template_dict[base] = (os.path.join(template_dir, file_name),)
    
    # Generate ensemble with template geometry data
    ensemble = crayon.nga.Ensemble(n_shells=n_shell)
    templates = {}
    for template_dict, func in ((templates_xyz, load_xyz_into_ensemble), (templates_gdv, load_gdv_into_ensemble)):
        templates.update(template_dict)
        func(ensemble, template_dict, rcut)
    save_data((ensemble, templates, rcut), ensemble_path)

def run_analyze(ensemble_path, traj_path, out_path, frames, box, radii, alpha, cutoff, do_txt, do_gdv, do_xyz, skip, amax):
    # Do analysis
    trajectories = {out_path: (traj_path, frames, box)}
    #print (trajectories) RMtest
    ensemble, templates, rcut = load_data(ensemble_path)
    #print (ensemble,templates,rcut)
    load_xyz_into_ensemble(ensemble, trajectories, rcut)
    result = analyze(ensemble, templates, trajectories, alpha=alpha, signature_cutoff=cutoff, skip_count=skip, amax=amax)
    #print ('aaaaa',cutoff,amax)
 
    # Make output
    if do_txt:
        print_table(result, out_path + ".txt")
    if do_gdv:
        print_gdv(result, out_path +TAG+".gdv")
    if do_xyz:
        write_xyz(result, ".", radii)

def main():
    parser = argparse.ArgumentParser(description="Templated neighborhood graph analysis")
    subparsers = parser.add_subparsers(description="Use %(prog)s {command} --help for more information")

    subparser = subparsers.add_parser("template", description="Reads templates and generates a data file for later use")
    subparser.add_argument("templates", help="directory containing .xyz and/or .gdv as templates to read", metavar="directory")
    subparser.add_argument("ensemble", help="data file containing template structural information", metavar="file")
    subparser.add_argument("-r", "--r-cut", type=float, default=None, metavar="r",
        help="global cutoff to use for neighbor calculations with this data file")
    subparser.add_argument("-n", "--n-shells", type=int, default=1, metavar="n",
        help="number of neighbor shells to take into account (default is 1)")
    def template_command(arguments):
        load_templates(arguments.templates, arguments.ensemble, arguments.r_cut, arguments.n_shells)
    subparser.set_defaults(dispatch_command=template_command)

    subparser = subparsers.add_parser("analyze", description="Performs analysis on a trajectory")
    subparser.add_argument("ensemble", help="data file generated with template command", metavar="file")
    subparser.add_argument("trajectory", help=".xyz file containing trajectory data", metavar="file")
    subparser.add_argument("output", help="prefix for all files with analysis results", metavar="prefix")
    subparser.add_argument("-f", "--frames", type=int, nargs=3, help="range(start, stop, step) of frames to read",
        metavar=("start", "stop", "step"))
    subparser.add_argument("-b", "--box", type=float, nargs=3, help="box size", metavar=("x", "y", "z"), required=True)
    subparser.add_argument("-r", "--radius", action="append", default=[], nargs=2, help="particle type radius",
        metavar=("type", "radius"))
    subparser.add_argument("-a", "--alpha", type=float, default=0.25,
        help="distance scaling exponent for diffusion mapping", metavar="exponent")
    subparser.add_argument("-c", "--cutoff", type=float, default=ANALYZE_CUTOFF,
        help="cutoff for particle count to classify structure separately", metavar="count")
    subparser.add_argument("-t", "--no-write-txt", action="store_true",
        help="skip writing (output).txt file containing particle counts")
    subparser.add_argument("-g", "--no-write-gdv", action="store_true",
        help="skip writing (output).gdv file containing graphlet signature data")
    subparser.add_argument("-x", "--no-write-xyz", action="store_true",
        help="skip writing (output)_*.xyz files containing trajectory information for visualization")
    subparser.add_argument("-s", "--skip-frames", type=int, default=0, metavar="count",
        help="specify some number of frames to include in diffusion mapping only (excluding them from classification)")
    subparser.add_argument("-m", "--use-max", action="store_true",
        help="use the maximum instead of the mean number of particles when finding significant structures")
    def analyze_command(arguments):
        run_analyze(arguments.ensemble, arguments.trajectory, arguments.output, range(*arguments.frames)
            if arguments.frames is not None else None, numpy.array(arguments.box),
            {key: float(value) for key, value in arguments.radius}, arguments.alpha, arguments.cutoff,
            not arguments.no_write_txt, not arguments.no_write_gdv,
            not arguments.no_write_xyz, arguments.skip_frames,arguments.use_max)
    subparser.set_defaults(dispatch_command=analyze_command)

    arguments = parser.parse_args()
    if "dispatch_command" not in arguments:
        parser.print_help()
        sys.exit()
    arguments.dispatch_command(arguments)

if __name__ == "__main__":
    TAG = 'same' #performe analysis for same type particles 
    main()
    TAG = 'diff' #performe analysis for different type particles 
    main()
    TAG = 'all' #performe analysis for all particles regardless of type
    main()
